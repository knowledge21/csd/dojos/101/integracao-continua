import math


def deixar_so_com_duas_casas_decimais(comissao):
    return round(comissao, 2)

def calcula_comissao(venda):
    comissao = venda * 0.05
    return deixar_so_com_duas_casas_decimais(comissao)
