# Dojo de Introdução a Testes Automatizados com TDD

## Índice
  - [Introdução](#introdução)
  - [1. Tutorial para configurar o seu ambiente](#1-tutorial-para-configurar-o-seu-ambiente)
    - [1.1 Baixar e instalar o Pycharm](#11-baixar-e-instalar-o-pycharm)
    - [1.2 Baixar o codigo fonte para seu computador](#12-baixar-o-codigo-fonte-para-seu-computador)
    - [1.3 Baixar e instalar o Python](#13-baixar-e-instalar-o-python)
    - [1.4 Rodar os testes e correr pro abraço](#14-rodar-os-testes-e-correr-pro-abraço)
  - [2. O Dojo!](#2-o-dojo)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Introdução

Este é um Coding Dojo para quem quer começar a praticar testes automatizados.

A linguagem escolhida foi Python, mas isso é quase indiferente para a essência do que o Dojo pretende ensinar.

Antes do desafio é importante montar o ambiente em que o Dojo será realizado.

## 1. Tutorial para configurar o seu ambiente

Poderíamos utilizar o bloco de notas para escrever nossos códigos, mas tem ferramentas que facilitam muito mais a nossa vida na hora de "codar".

### 1.1 Baixar e instalar o Pycharm

A sugestão aqui é que você utilize o Pycharm, que é uma ferramenta para desenvolvimento de código em Python de uma empresa bem boa chamada JetBrains.

* **Baixe o Pycharm versão Community, disponível [neste link](https://www.jetbrains.com/pt-br/pycharm/download/#section=windows).**

![Onde baixar o Pycharm](https://i.ibb.co/YjM8jvM/pycharm-download.png)

### 1.2 Baixar o codigo fonte para seu computador

*Obs: As instruções deste passo são para Windows, mas não é tão complicado tentar fazer em Mac ou Linux. O pulo do gato está em usar o terminal do Mac ou do Linux ao invés do Power Shell (que é o do Windows). ;)*

Para baixar o código fonte deste repositório na nuvem (GitLab) para o seu computador, utilizaremos um pouco de linha de comando. Preparada? :)

O aplicativo que vamos utilizar para entrar com os comandos que vão baixar o projeto se chama Power Shell, e já vem instalado no Windows. \o/

Talvez eu chame o Power Shell aqui algumas vezes de "terminal", que é um termo genérico para este tipo de aplicativo de linha de comando.

* Para abri-lo, no menu inicial do Windows, procure por "Power Shell":

![Procurando o Power Shell](https://i.ibb.co/W2J3Y2b/powershell.png)

Provavelmente o PowerShell vai abrir apontando para a pasta em que ficam todos os seus arquivos de usuário do Windows (no meu caso, ```C:\Users\k21lula```).

A primeira coisa que vamos fazer é criar uma pasta nova para colocar qualquer projeto com o qual formos trabalhar daqui pra frente. O nome da pasta será ```Coding```. Pq ```Coding```? Porque eu quis. Mas poderia ser até ```Batatinha``` se você quisesse.

Eu poderia muito bem criar essa pasta pelas próprias janelas do Windows, sem usar linha de comando. Mas já quero ir te habituando com linhas de comando. ;)

* **Então o comando que você vai digitar no Power Shell é: ```mkdir Coding```** 

*(mkdir é de Make Directory, eu acho, hihi)*

Perceba que se você entrar no diretório do seu usuário pelo navegador de pastas do Windows que você usa no dia-a-dia (no meu caso em ```C:\Users\k21lula```, você consegue ver nas pastas do Windows o diretório que criamos por linha de comando:

![Pasta Coding criada](https://i.ibb.co/tJc75QW/mkdir.png)

Agora que a pasta Coding está criada, vamos entrar nela, mas usando o terminal ao invés do clique duplo no Explorador de Arquivos com o qual você está acostumado. 

* **Para isso, digite no terminal (o Power Shell) ```cd Coding```**

*(pesquisei aqui e aprendi que ```cd```significa Change Directory)*

Nessas horas você já deve estar começando a se sentir meio hacker mandando essas linhas de comando marota, né?

![Dentro da pasta Coding](https://i.ibb.co/WHNsjjY/cd.png)

Antes de mandar a linha de comando definitiva que vai baixar o nosso projeto do repositório para o computador precisamos instalar a ferramenta que faz essa comunicação entre o repositório (GitLab) e o seu computador. O nome dessa ferramenta é Git. 

- **Baixe o Git [aqui](http://git-scm.com/download/win) e instale-o como você instalaria qualquer outro programa no Windows.**

  - **Pode ir dando "Next" na instalação sem alterar nada, até finalizar a instalação. Sem medo, manda ver!"**

- Depois de instalar o Git, feche o Powershell e abra-o de novo. 

- Navegue até a pasta "Coding" usando o ```cd Coding``` *(esse comando vc já conhece né, hehe)*

Agora o gran finale (pelo menos dessa etapa): Vamos baixar o código fonte do nosso repositório (GitLab). 

- **Para isso, digite no PowerShell a linha de comando ```git clone https://gitlab.com/knowledge21/csd/dojos/101/unit-testing.git```**

Essa linha de comando está literalmente falando ao Git (que faz a ponte do seu computador com o repositório) para clonar na sua máquina o conteúdo desse repositório aqui. Agora você consegue ver dentro da sua pastinha ```Coding``` os mesmos arquivos que estão aqui no repositório. \o/

![Git clone do sucesso](https://i.ibb.co/fx4dLxc/gitclone.png)

Se algum dia você quiser clonar algum outro repositório (seja do GitLab ou do GitHub ou do BitBucket ou qualquer outro repositório de código fonte) é só você procurar pelo endereço do repositório e mandar esse ```git clone <endereço do repo>```. 

*(dica: procure sempre pelo endereço que comece com ```https```, e por enquanto fuja dos endereços que comecem com ```git@```)*

![Um outro exemplo de como pegar o endereço de um repositório no github](https://i.ibb.co/K0Kct1Z/clone.png)

### 1.3 Baixar e instalar o Python

Agora, antes de abrir o PyCharm e fazer tudo funcionar vamos instalar o Python no seu computador. É suave, se liga:

- **Entre [nesse link](https://www.python.org/downloads/) e faça o download**
- Faça a instalação naquele esquema "Next, Next, Finish" de sempre, MAS:
    - **Não esqueça de marcar o checkbox de ```Add Python 3.9 to PATH```, como mostra a imagem abaixo**

![Marca a caixinha, marca](https://i.ibb.co/DCjs1Wx/Python.png)

### 1.4 Rodar os testes e correr pro abraço

Agora que temos:

- [Dojo de Introdução a Testes Automatizados com TDD](#dojo-de-introdução-a-testes-automatizados-com-tdd)
  - [Índice](#índice)
  - [Introdução](#introdução)
  - [1. Tutorial para configurar o seu ambiente](#1-tutorial-para-configurar-o-seu-ambiente)
    - [1.1 Baixar e instalar o Pycharm](#11-baixar-e-instalar-o-pycharm)
    - [1.2 Baixar o codigo fonte para seu computador](#12-baixar-o-codigo-fonte-para-seu-computador)
    - [1.3 Baixar e instalar o Python](#13-baixar-e-instalar-o-python)
    - [1.4 Rodar os testes e correr pro abraço](#14-rodar-os-testes-e-correr-pro-abraço)
  - [2. O Dojo!](#2-o-dojo)
    - [2.1 O problema a ser resolvido](#21-o-problema-a-ser-resolvido)
    - [2.2 Por onde começar](#22-por-onde-começar)

Chegou a hora: Bora rodar o rolê!

- Para isso, abra o Pycharm
- Selecione a opção "Open"
- Selecione a pasta unit-testing que está dentro de Coding (que vc baixou do repositório na [etapa 1.2 deste tutorial](#12-baixar-o-codigo-fonte-para-seu-computador))
- Clique em OK

![No PyCharm Selecione "Open", seleciona a pasta unit-testing e clique em OK](https://i.ibb.co/R0nz2kd/Pycharm-open.png)

Agora com o projeto aberto, vamos rodar os testes! \o/

- Para isso, abra o arquivo ```test_calculadora_comissao.py``` que tá ali no canto esquerdo
- Depois clique em "Terminal", lá embaixo
- Depois digite no terminal que abriu ```python -m unittest discover```

![Pycharm rodando com sucesso](https://i.ibb.co/2ykHmb6/python-run.png)

Pronto! Seu ambiente foi configurado com sucesso!

## 2. O Dojo!

Com o ambiente configurado, agora é a hora de entrar de cabeça no desafio!

### 2.1 O problema a ser resolvido

Neste Dojo o problema a ser resolvido é basicamente conseguir o feedback dos testes sem precisar rodá-los manualmente.

### 2.2 Por onde começar

Faça um fork desse projeto para algum repositório seu. Pegue [esse exemplo](https://gitlab.com/knowledge21/csd/dojos/pt-br/python/cd/-/blob/CI/.gitlab-ci.yml) de gitlab-ci.yml e dê uma limpada para deixar só a parte de testes.

Aí é só subir o ```.gitlab-ci.yml``` no seu repositório e correr pro abraço!