from unittest import TestCase
import sistema_vendas.calculadora_comissao as calculadora_comissao


class TestCalculadoraComissao(TestCase):

    def teste_calculo_de_comissao_para_venda_de_500_reais_retorna_25(self):
        # ARRANGE
        valor_venda = 500
        resultado_esperado = 25

        # ACT
        resultado_calculado = calculadora_comissao.calcula_comissao(valor_venda)

        # ASSERT
        self.assertEqual(resultado_calculado, resultado_esperado)

    def teste_calculo_de_comissao_para_venda_de_1000_reais_retorna_50(self):
        # ARRANGE
        valor_venda = 1000
        resultado_esperado = 50

        # ACT
        resultado_calculado = calculadora_comissao.calcula_comissao(valor_venda)

        # ASSERT
        self.assertEqual(resultado_calculado, resultado_esperado)

    def teste_calculo_de_comissao_para_venda_de_10000_reais_retorna_500(self):
        # ARRANGE
        valor_venda = 10000
        resultado_esperado = 500

        # ACT
        resultado_calculado = calculadora_comissao.calcula_comissao(valor_venda)

        # ASSERT
        self.assertEqual(resultado_calculado, resultado_esperado)

    def teste_calculo_de_comissao_para_venda_de_55_59_reais_retorna_2_77(self):
        # ARRANGE
        valor_venda = 55.59
        resultado_esperado = 2.77

        # ACT
        resultado_calculado = calculadora_comissao.calcula_comissao(valor_venda)

        # ASSERT
        self.assertEqual(resultado_calculado, resultado_esperado)
